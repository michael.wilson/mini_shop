module AdminsHelper
  def active?(activated)
    activated ? "Deactivate" : "Activate"
  end
end
