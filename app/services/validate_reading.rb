class ValidateReading
  
  attr_reader :reading, :response, :old_reading

  def initialize(reading:)
    @reading = reading
    @response = OpenStruct.new(error: [], success: false)
    @old_reading = get_users_last_reading
  end

  def call
    validate_kwh
    compare_readings
    create_read
    response
  end

  def compare_readings
    if reading.kwph >= constructed_reading && reading.valid?
      response.success = true
    else
      response.error << ["Reading must be greater than last reading"]
    end
  end

  def validate_kwh
    response.error << reading.errors.messages[:kwh] unless reading.valid?
  end

  def constructed_reading
    old_reading.present? ? old_reading.kwph : nil.to_f
  end

  def create_read
    CreateRead.persist!(reading) if response.success
  end

  private

  def get_users_last_reading
    Reading.users_readings(reading.user_id).last
  end
end


class CreateRead
  def self.persist!(reading)
    reading.save!
  end
end
