class Auth::CreateUser

  attr_reader :role, :user_params, :user

  def initialize(user_params:, role:)
    @role = role
    @user_params = user_params
  end

  def call
    add_role
    build_new_user
    save_valid_user
    user
  end

  def add_role
    @user_params[:role] = role
  end

  def build_new_user
    @user = User.new(user_params)
  end

  def save_valid_user
    user.save if user.valid?
  end
end
