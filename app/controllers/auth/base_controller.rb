class Auth::BaseController < ApplicationController
  attr_reader :user

  def find_user
    @user = User.find_by(id: params[:id])
  end

  def authenticate
    redirect_to root_path unless authenticated?
  end

  def authenticate_admin
    redirect_to root_path unless admin_authenticated?
  end

  def log_in(user)
    session[:user_id] = user.id
  end

  def log_out
    session.delete(:user_id)
  end

  def destroy
    log_out
    redirect_to root_path
  end

  def current_user
    User.find_or_initialize_by(id: session[:user_id])
  end

  def authenticated?
    user.id == session[:user_id] || current_user.admin?
  end

  def admin_authenticated?
    user.id == session[:user_id] && user.admin?
  end

  def customer_or_admin_redirect(user)
    if user.admin?
      redirect_to auth_admin_path(user.id)
    else
      redirect_to auth_customer_path(user.id)
    end
  end  

end
