class Auth::AdminsController < Auth::UsersController
  
  before_action :find_user, :authenticate_admin
  before_action :get_all_customers, only: [:show, :customers]
  before_action :find_customer, only: [:invalidate_customer_icp,
                                       :verify_customer_icp]


  def active_customers
    @customers = User.active
  end

  def inactive_customers
    @customers = User.inactive
  end 

  def verify_customer_icp
    previous_page if @customer.activate
  end

  def invalidate_customer_icp
    previous_page if @customer.invalidate
  end

  private
  
  def get_all_customers
    @customers = User.customers
  end

  def previous_page
    redirect_back(fallback_location: auth_admin_path(@user))
  end

  def find_customer
    @customer = User.find_by_id(params[:customer_id])
  end
end
