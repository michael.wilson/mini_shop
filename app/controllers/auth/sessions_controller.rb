class Auth::SessionsController < Auth::UsersController
  
  def create
    user = User.find_by(name: params[:session][:name].downcase)
    if user
      log_in(user)
      flash[:notice] = ""
      customer_or_admin_redirect(user)
    else
      render 'auth/sessions/new', notice: "Incorrect Login"
    end
  end
end
