class Auth::CustomersController < Auth::UsersController

  before_action :find_user, :authenticate

  def update
    @user.update({ active: false }.merge(user_params))
    if @user.save
      redirect_to auth_customer_path(@user)
    else
      render '/auth/customers/edit'
    end
  end

  def readings
    
  end
  
end
