class Auth::UsersController < Auth::BaseController

  def new
    @user = User.new
  end

  def create
    @user = Auth::CreateUser.new(
      user_params: user_params,
      role: User::CUSTOMER
    ).call

    if @user.valid?
      log_in(@user)
      flash[:notice] = "Successfully created account"
      redirect_to auth_customer_path(@user)
    else
      render '/auth/users/new'
    end
  end

  private

  def user_params
    params.require(:user).permit(:name, :address, :icp, :role, :active)
  end
end
