class ReadingsController < Auth::BaseController

  before_action :find_customer, only: [:new, :create]

  def new
    @reading = @customer.readings.build
  end

  def create
    @reading = @customer.readings.build(readings_params)
    read = ValidateReading.new(reading: @reading).call

    if read.success
      redirect_to customer_readings_path(@customer.id)
    else
      redirect_to new_auth_customer_reading_path(@customer.id), notice: read.error.flatten
    end
  end

  def destroy
    reading = Reading.find(params[:id])
    reading.delete
    redirect_back(fallback_location: root_path)
  end

  private

  def find_customer
    @customer = User.find(params[:customer_id])
  end

  def readings_params
    params.require(:reading).permit(:kwh)
  end
end
