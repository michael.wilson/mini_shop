class Reading < ApplicationRecord
  belongs_to :user

  validates :kwh, presence:true, numericality: {only_float: true}

  scope :users_readings, -> (user_id) { 
    where(user_id: user_id).order(created_at: :asc) 
  }

  def local_time
    created_at.localtime.strftime("%I:%M%p")
  end

  def local_date
    created_at.localtime.strftime("%d/%m/%y")
  end

  def kwph
    kwh.to_f
  end
end
