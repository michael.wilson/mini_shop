class User < ApplicationRecord

  CUSTOMER = "customer"
  ADMIN = "admin"

  has_many :readings
  before_save :downcase_name
  validates_presence_of :name
  validates_presence_of :address,
                        :icp,
                        unless: :admin?

  validates :name, uniqueness: true
  validates :icp, 
    format: { with: /\A[0-9]+\z/, message: "must contain only numbers" },
    length: { is: 9 },
    unless: :admin?

  scope :active, -> { where(active: true, role: CUSTOMER) }
  scope :inactive, -> { where(active: false, role: CUSTOMER) }
  scope :customers, -> { where(role: CUSTOMER) }

  def downcase_name
    self.name.downcase!
  end
  
  def admin?
    role == ADMIN
  end

  def customer?
    role == CUSTOMER
  end

  def activate
    update_attribute(:active, true)
  end

  def invalidate
    update_attribute(:active, false)
  end
end
