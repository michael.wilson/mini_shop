class ChangeUserIcpType < ActiveRecord::Migration[5.1]
  def change
    change_column :users, :icp, :string
  end
end
