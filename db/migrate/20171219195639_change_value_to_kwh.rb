class ChangeValueToKwh < ActiveRecord::Migration[5.1]
  def change
    rename_column :readings, :value, :kwh
  end
end
