class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name
      t.integer :icp
      t.string :address
      t.string :role, :null => false

      t.timestamps
    end
  end
end
