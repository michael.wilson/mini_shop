FactoryBot.define do
  factory :user do
    name "MyString"
    role "MyString"
    icp 1
    address "MyString"
  end
end
