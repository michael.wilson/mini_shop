require 'rails_helper'

RSpec.describe Auth::CustomersController, type: :controller do

  fixtures :users
  let (:customer) { users(:customer) }

  let (:valid_session) { { user_id: customer.id } }

  describe "GET #update" do
    let (:new_attributes) { { address: "lawler"} }
    context "when a customer is logged in" do
      it "updates attributes" do
        put :update, params: {
          id: customer.id,
          user: new_attributes
        }, session: valid_session
        customer.reload
        expect(customer.address).to eq(new_attributes[:address])
      end
    end

    context "when customer updates there profile" do
      it "changes the state of activation to false" do
        customer.active = true
        put :update, params: {
          id: customer.id,
          user: new_attributes
        }, session: valid_session
        customer.reload
        expect(customer.active).to eq(false)
      end
    end
  end
end
