require 'rails_helper'

RSpec.describe Auth::UsersController, type: :controller do

  fixtures :users
  let (:user) { users(:user)}

  let(:valid_attributes) do
    {
      name: "michael",
      address: "7 Mystery Lane",
      icp: 123123123,
    }
  end

  let(:invalid_attributes) do
    {
      name: "michael",
      address: "7 Mystery Lane",
    }
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new, params: {}
      expect(response.status).to eq(200)
    end
  end

  describe "GET #create" do
    context "with valid params" do
      it "Creates a new user" do
        expect {
          post :create, params: {user: valid_attributes}
        }.to change(User, :count).by(1)
      end
    end

    context "with invalid params" do
      it "redirects to new page" do
        post :create, params: {user: invalid_attributes}
        expect(response).to render_template(:new)
      end
    end
  end  
end
