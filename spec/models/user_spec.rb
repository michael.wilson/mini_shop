require 'rails_helper'

RSpec.describe User, type: :model do
  
  let (:icp) { "123456789" }
  let (:id) { 5 }
  let (:user_name) { "michael" }
  let (:address) { "6 Mystery Lane" }
  let (:role) { "user" }
  let (:user) do
    User.new(
    id: id,
    name: user_name,
    address: address,
    icp: icp,
    role: role
    )
  end

  let (:user_two) do 
    User.new(
    name: user_name,
    address: "888 Xmas Cove",
    icp: icp
    )
  end

  context 'user has all attributes' do
    it 'has an icp attribute' do
      expect(user.address).to eq("6 Mystery Lane")
    end

    it 'has a name attribute' do
      expect(user.name).to eq("michael")
    end

    it 'has a address attribute' do
      expect(user.icp).to eq("123456789")
    end

    it 'has a role attribute' do
      expect(user.role).to eq("user")
    end
  end

  context 'validates presence of attributes' do
    it 'name' do
      user.name = nil
      expect(user).to_not be_valid
    end

    it 'address' do
      user.address = nil
      expect(user).to_not be_valid
    end

    it 'icp' do
      user.icp = nil
      expect(user).to_not be_valid
    end

    it 'icp length' do
      expect(user).to be_valid
    end
  end

  describe 'validates name' do
    context 'name already exists' do

      before do
        user.save!
      end

      let (:id) { 6 }

      it 'invalid' do
        user_two.save
        error = user_two.errors.messages[:name].first
        expect(error).to include("already been taken")
      end
    end
  end

  describe 'validations for ICP' do
    context 'correct icp given' do

      let (:icp) { "123456789" }

      it 'has the right amount of numbers' do
        expect(user).to be_valid
      end

      it 'has right amount of numbers but in string' do
        expect(user.icp).to eq("123456789")
      end
    end

    context 'not enough numbers given' do

      let (:icp) { "1234" }

      it "is invalid" do
        user.save
        error = user.errors.messages[:icp].first
        expect(error).to include("wrong length (should be 9 characters)")
      end
    end

    context 'too many numbers given' do

      let (:icp) { "12321413213" }

      it 'invalid' do
        user.save
        expect(user.errors.messages).not_to be_empty
      end      
    end

    context 'takes in zeros as a valid icp' do

      let (:icp) { "000000000" }

      it 'valid' do
        user.save
        expect(user.icp).to eq("000000000")
      end      
    end

    context 'only takes in numbers for a valid icp' do

      let (:icp) { "00d000x00" }

      it 'valid' do
        user.save
        error = user.errors.messages[:icp].first
        expect(error).to include("must contain only numbers")
      end      
    end    
  end

  context 'creates user as a default role' do

    it 'has the user role' do
      expect(user.role).to eq('user')
    end
  end

  describe 'admin authentication' do

    let (:icp) { nil }

    let (:address) { nil }

    context 'admin does not need an icp or address' do
      
      let (:role) { "admin" }

      it 'creates an admin' do
        expect(user).to be_valid
      end
    end
  end

  describe 'username downcasing' do
    context 'downcases name before saving user' do

      let (:user_name) { "mICHAElz" }

      it 'has a downcased username string' do
        user.save!
        expect(User.last.name).to eq("michaelz")
      end
    end
  end
end
