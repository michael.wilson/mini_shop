require 'rails_helper'

RSpec.describe Reading, type: :model do

  let (:user) do
    User.new(
    id: 1,
    name: "Michael",
    address: "8 Mystery Lane",
    icp: "123123123",
    role: "customer"
    )
  end

  let (:readings) do 
    user.readings.new(kwh: 422.00)
  end

  context 'readings has a user' do
    it 'returns true' do
      expect(readings.user).to eq(user)
    end
  end

  context 'validates presence' do
    it 'valid kwh' do
      expect(readings).to be_valid
    end

    it 'invalid kwh' do
      readings.kwh = nil
      expect(readings).to_not be_valid
    end
  end  
end
