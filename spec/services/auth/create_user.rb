require 'rails_helper'

RSpec.describe Auth::CreateUser, type: :services do

  let(:admin_role) { "admin" }
  let(:customer_role) { "customer" }
  let(:params) do
    {
      name: "Hank",
      address: "2017 Hanksom Lane",
      icp: "999000111",
      role: "",
      active: false
    }
  end

  context 'creates user' do
    subject { described_class.new(params, customer_role) }

    it "adds a user to the User table" do
      expect { subject.call }.to change { User.count }.from(0).to(1)
    end
  end

  context 'customer role' do
    subject { described_class.new(params, customer_role) }

    it 'has a role of customer' do
      subject.add_role
      expect(params[:role]).to eq("customer")
    end

    it 'is valid' do
      subject.new_user
      expect(subject.valid_user?).to eq(true)
    end

    it "returns a user as a customer" do
      subject.call
      expect(User.last.role).to eq("customer")
    end
  end

  context 'admin role' do
    subject { described_class.new(params, admin_role) }

    it 'has a role of admin' do
      subject.add_role
      expect(params[:role]).to eq("admin")
    end

    it 'is valid' do
      subject.new_user
      expect(subject.valid_user?).to eq(true)
    end

    it "returns a user as a customer" do
      subject.call
      expect(User.last.role).to eq("admin")
    end
  end
end
