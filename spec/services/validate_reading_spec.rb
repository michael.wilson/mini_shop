require 'rails_helper'

RSpec.describe ValidateReading, type: :services do

  let (:reading) do
    Reading.new(
      user_id: 4,
      kwh: 333.33
    )
  end

  let (:user) do
    User.new(
    id: 4,
    name: "michael",
    address: "7 Mystery Lane",
    icp: 222333111,
    role: "customer"
    )
  end

  let(:last_reading) do
    user.readings.build(
      kwh: 222.55,
      user_id: 4
    )
  end

  before do
    user.save
    last_reading.save
  end

  subject { described_class.new(reading: reading) }

  context 'gets the last reading' do
    it "has the correct last reading" do
      expect(subject.old_reading).to eq(last_reading)
    end
  end

  describe 'checks the last reading against the new reading' do
    context 'valid reading' do
      before do
        expect(subject).to receive(:constructed_reading).and_return(133.11)
        subject.compare_readings
      end

      it "has a response of success to be true" do
        expect(subject.response.success).to eq(true)
      end

      it "has no errors" do
        expect(subject.response.error.length).to eq 0
      end
    end

    context 'invalid reading' do
      before do
        expect(subject).to receive(:constructed_reading).and_return(633.11)
        subject.compare_readings
      end

      it "has a response of success to be false" do
        expect(subject.response.success).to eq(false)
      end

      it "has errors" do
        expect(subject.response.error.length).to be > 0
      end
    end
  end

  context 'old reading is nil' do
    it "returns a float class" do
      allow(subject).to receive(:old_reading).and_return(nil)
      expect(subject.constructed_reading).to eq(0.0)
    end

    it "returns a float class" do
      expect(subject.constructed_reading).to eq(222.55)
    end
  end

end
