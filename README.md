# MiniShop

Minishop for my mini power company

### Installing

bundle gems

```
bundle install
```

## Running the tests

Rspec installed as well as Cucumber tests

```
bundle exec rspec
```

```
bundle exec cucumber
```

## Built With

* [Rails 5.0](http://guides.rubyonrails.org/v5.0/) - Framework
* [Ruby 2.4.2](https://www.ruby-lang.org/en/)


## Authors

* **Michael Wilson** - *Initial work*
