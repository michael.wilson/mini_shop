Rails.application.routes.draw do

  root 'home#index'

  namespace :auth do
    resources :customers do
      resources :readings, only: [:create, :new, :destroy], controller: '/readings'
    end
    resources :admins, only: [:new, :show]
    resources :users, only: [:new, :create, :show]
  end

  get 'auth/admins/:id/active', to: 'auth/admins#active_customers'
  get 'auth/admins/:id/inactive', to: 'auth/admins#inactive_customers'
  get 'auth/admins/:id/customers', to: 'auth/admins#customers'

  get 'auth/customers/:id/readings', to: 'auth/customers#readings', as: 'customer_readings'

  patch 'auth/admins/:id/verify_customer_icp' => "auth/admins#verify_customer_icp", :as => :verify_customer_icp
  patch 'auth/admins/:id/invalidate_customer_icp' => "auth/admins#invalidate_customer_icp", :as => :invalidate_customer_icp
  
  get 'login', to: 'auth/sessions#new'
  post 'login', to: 'auth/sessions#create'
  delete 'logout', to: 'auth/sessions#destroy'
end
