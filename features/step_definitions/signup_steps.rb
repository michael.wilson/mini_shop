Given(/^I am on the signup page$/) do
  visit new_auth_user_path
end

And(/^I enter my details$/) do
  fill_in "user[name]", with: 'fluxing heck'
  fill_in "user[address]", with: "7 Mystery Lane"
  fill_in "user[icp]", with: 123456789
end

When(/^I submit my details$/) do
  find('.btn').click
end

Then(/^I am logged in and redirected to my users page$/) do
  expect(page).to have_content("Successfully created account")
end

Given(/^There are other users with the same name when I try and register$/) do
  user = User.new(role: "customer", name: 'fluxing heck', address: '6 Uniq Cove', icp: 123123123)
  user.save
  visit new_auth_user_path
end

Then(/^I have a name taken error$/) do
  expect(page).to have_content("Name has already been taken")
end


