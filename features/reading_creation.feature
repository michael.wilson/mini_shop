Feature: Create Readings Feature
As a Admin
I want to add a reading to a customer

Scenario: Creates a reading
  Given I am on the readings new page and an admin
  And I enter a reading
  When I submit the reading
  Then I can see the customers new reading
