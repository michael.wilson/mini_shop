Feature: Sign up Feature
As a User
I want to signup

Scenario: Signs up a user
  Given I am on the signup page
  And I enter my details
  When I submit my details
  Then I am logged in and redirected to my users page

Scenario: Another User with the same name already exists
  Given There are other users with the same name when I try and register
  And I enter my details
  When I submit my details
  Then I have a name taken error
